package dev.rehm.controllers;

import dev.rehm.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/shopping-cart")
public class CartController {

    List<Item> shoppingCart = new ArrayList<>();

    @Autowired
    RestTemplate restTemplate;

    /*
    @Autowired
    DiscoveryClient discoveryClient;

    @PostMapping("/items")
    public List<Item> addItemToCart(@RequestParam("id") int id){
        // discovery client is used to obtain consul's registry, a stream is used to process this data
        Optional<URI> requestUriOpt = discoveryClient.getInstances("market-service").stream()
                .map(ServiceInstance::getUri).peek(System.out::println)
                .findFirst();
        // the stream operation results in an optional uri
        if(requestUriOpt.isPresent()){
            // if there is a valid uri returned from the registry, we can make a request to it
            URI requestUri = requestUriOpt.get();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "admin-token");
            HttpEntity<Item> request = new HttpEntity<>(headers);
            ResponseEntity<Item> responseEntity = restTemplate.exchange(requestUri.resolve("/items/"+id), HttpMethod.GET
                    ,request,
                    Item.class);
            System.out.println("response retrieved: "+ responseEntity);
            if(responseEntity.getBody()!=null){
                // if we get back a valid object from our request, we add it to the cart
                shoppingCart.add(responseEntity.getBody());
            }
        }
        return shoppingCart;
    }
     */

    @PostMapping("/items")
    public List<Item> addItemToCart(@RequestParam("id") int id){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "admin-token");
        HttpEntity<Item> request = new HttpEntity<>(headers);
        ResponseEntity<Item> responseEntity = restTemplate.exchange("http://market-service/items/"+id, HttpMethod.GET
                ,request, Item.class);
        System.out.println("response retrieved: "+ responseEntity);
        if(responseEntity.getBody()!=null){
            // if we get back a valid object from our request, we add it to the cart
            shoppingCart.add(responseEntity.getBody());
        }
        return shoppingCart;
    }

}
